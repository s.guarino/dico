import os
import igraph
import pickle
from argparse import ArgumentParser
import numpy as np
from copy import deepcopy

def read_tweets(fname):
    edges = []
    tweets = {}
    with open(fname, 'r') as f:
        for line in f:
            s,t,attrs = line.split(' ',2)
            attrs = eval(attrs)
            edges.append([s,t,attrs])
            these_tweets = list(attrs['tweet_id'])
            for t in these_tweets:
                tweets.setdefault(t,[]).append(len(edges)-1)
    return edges,tweets


def remove_tweets(edges, tweets, rate):
    rands = np.random.random(len(tweets))
    tweets_to_remove = np.asarray(list(tweets.keys()))[rands<rate].tolist()
    for t in tweets_to_remove:
        for i in tweets[t]:
            attrs = edges[i][2]
            if attrs['weight'] == 1:
                edges[i] = None
            else:
                attrs['weight'] -= 1
                attrs['tweet_id'].remove(t)
    return [e for e in edges if e is not None]


def build_graph(edgelist):
    uid2vid = {}
    vid2uid = []
    n = 0
    edges = []
    ws = []
    for s,t,attrs in edgelist:
        sid = uid2vid.setdefault(s,n)
        if sid == n:
            vid2uid.append(s)
            n = n+1
        tid = uid2vid.setdefault(t,n)
        if tid == n:
            vid2uid.append(t)
            n = n+1
        edges.append((sid,tid))
        ws.append(attrs['weight'])
    G = igraph.Graph(edges=edges, directed=True, edge_attrs={'weight':ws},
                     vertex_attrs={'name':vid2uid})
    return G


def parse_args(): # -> tuple[str, str, int]:
    parser = ArgumentParser()
    parser.add_argument("dataset", type=str)
    parser.add_argument("rate", type=float)
    parser.add_argument("iters", type=int)
    args = parser.parse_args()
    return args.dataset, args.rate, args.iters


def main() -> None:
    dataset,rate,iters = parse_args()
    folder_name = f'{dataset}/retweet_graph_removal_rate_{rate}/'
    os.makedirs(folder_name, exist_ok=True)
    print(f'reading all available tweets for {dataset}')
    edges,tweets = read_tweets(f'{dataset}/global_RT_graph')
    print(f'{len(tweets)} tweets read, corresponding to {len(edges)} weighted edges')
    for i in range(iters):
        fname = os.path.join(folder_name, f'graph_{i:02}.pickle')
        if os.path.exists(fname):
            print(f'igraph file found at {fname}')
            continue
        print(f'building igraph object for {dataset} with removal rate {rate}')
        ecopy = deepcopy(edges)
        edgelist = remove_tweets(ecopy, tweets, rate)
        G = build_graph(edgelist)
        print(f'dumping graph at {fname}')
        with open(fname, 'wb') as f:
            pickle.dump(G, f)   


if __name__ == "__main__":
    main()

