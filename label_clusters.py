#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import sys
import igraph
import pickle


# In[2]:


def get_annotated_clusters(G, root = 'annotated_communities'):
    uid2vid = {v['name']:v.index for v in G.vs}
    m = ['na' for _ in range(G.vcount())]
    for f in os.listdir(root):
        cluster_name = f.split('_')[0]
        with open(os.path.join(root,f), 'r') as fin:
            d = eval(fin.read())
        users = []
        for u in d.keys():
            v = uid2vid.get(u)
            if v is not None:
                m[v] = cluster_name 
    G.vs['annotated'] = m
    return m


# In[3]:


def get_community_labels(G, proj='$V$'):
    VGs = igraph.VertexClustering.FromAttribute(G, proj).subgraphs()
    labels = {}
    for VG in VGs:
        com = VG.vs['$V$'][0]
        if com >= 0:
            count = Counter(VG.vs['annotated'])
            del count['na']
            a = sorted(count.items(), key=lambda x:x[1], reverse=True)
            if a[0][1]>2*a[1][1]:
                label = a[0][0]
            else:
                label = a[0][0]+'&'+a[1][0]
        labels[com] = label
    return labels


# In[ ]:


DATASETS = ['ita_elections', 'quirinale', 'crisi']


# In[ ]:


for dataset in DATASETS:
    G = igraph.read(f'{dataset}/retweet_graph_with_clusters.pickle')
    m = get_annotated_clusters(G, root='annotated_communities/')
    labels = get_community_labels(G)
    with open(os.path.join(dataset, 'community_labels.pickle'), 'wb') as f:
        pickle.dump(labels, f)


# In[ ]:





# In[ ]:




