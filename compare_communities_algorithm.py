import numpy as np
import pickle
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.metrics import adjusted_mutual_info_score as AMI
from sklearn.metrics import adjusted_rand_score as ARI
from sklearn.metrics import normalized_mutual_info_score as NMI
from igraph import compare_communities
# from sklearn.metrics import normvariation_of_information as VI
from label_clusters import get_annotated_clusters
# from cdlib.classes import NodeClustering
from collections import Counter

################### MACROS #####################
dataset = 'ita_elections'
get_mutual_info_scores = True
get_accuracy_scores = True
################################################

def precision(cm):
    return cm['TP']/max(1,(cm['TP']+cm['FP']))

def recall(cm):
    return cm['TP']/max(1,(cm['TP']+cm['FN']))

def specificity(cm):
    return cm['TN']/max(1,(cm['TN']+cm['FP']))

def VI(com1, com2):
    return compare_communities(com1, com2, method='vi')

def clean(com):
    ids = []
    labels = []
    for i,l in enumerate(com):
        if l!=-1:
            ids.append(i)
            labels.append(l)
    return ids,labels

def get_communities(labels, ids=None):
    if ids is None:
        ids = range(len(labels))
    coms = {}
    for i,l in zip(ids,labels):
        coms.setdefault(l,[]).append(i)
    return coms

def drop_small_clusters(labels, threshold=200):
    d = Counter(labels)
    to_drop = [h for h,u in d.items() if h!=-1 and u<200]
    v = np.array(labels)
    for h in to_drop:
        v[v==h] = -1
    return v

# read graph and clusters
with open(f'{dataset}/retweet_graph_with_clusters.pickle','rb') as f:
    G = pickle.load(f)
N = G.vcount()
with open(f'{dataset}/projection_clusters.pickle','rb') as f:
    proj_clusters = pickle.load(f)
with open(f'{dataset}/other_clusters.pickle','rb') as f:
    alg_clusters = pickle.load(f)
gt_labels = get_annotated_clusters(G, 'annotated_communities/')
gt_unique_labels = sorted(set(gt_labels))
gt_labels2ids = {l:i for i,l in enumerate([h for h in gt_unique_labels if h!='na'])}
gt_labels2ids['na'] = -1
gt_ids2labels = {i:l for l,i in gt_labels2ids.items()}
gt_labels = [gt_labels2ids[l] for l in gt_labels]
for k,v in alg_clusters.items():
    alg_clusters[k] = drop_small_clusters(v)    
all_clusters = {'annotated':gt_labels}
all_clusters.update(proj_clusters)
all_clusters.update(alg_clusters)
num_cluster_types = len(all_clusters)

# get mutual info scores
if get_mutual_info_scores:
    AMI_matrix = np.zeros((num_cluster_types,num_cluster_types))
    ARI_matrix = np.zeros((num_cluster_types,num_cluster_types))
    NMI_matrix = np.zeros((num_cluster_types,num_cluster_types))
    VI_matrix = np.zeros((num_cluster_types,num_cluster_types))
    i = 0
    for k,v in all_clusters.items():
        # remove -1, i.e., unlabeled ids
        ids,labels = clean(v)
        j = 0
        for h,u in all_clusters.items():
            l = np.asarray(u)[ids]
            AMI_matrix[i,j] = AMI(labels,l)
            ARI_matrix[i,j] = ARI(labels,l)
            NMI_matrix[i,j] = NMI(labels,l)
            for a,b in np.ndenumerate(np.unique(l)):
                l[l==b] = a
            VI_matrix[i,j] = compare_communities(labels,l,method='vi')
            # NodeClustering(get_communities(labels)).variation_of_information(NodeClustering(get_communities(np.asarray(u)[ids])))
            j += 1
        i += 1
    AMI_df = pd.DataFrame(AMI_matrix, index=all_clusters.keys(), columns=all_clusters.keys())
    AMI_df.to_pickle(f'{dataset}/AMI_df.pickle')
    ARI_df = pd.DataFrame(ARI_matrix, index=all_clusters.keys(), columns=all_clusters.keys())
    ARI_df.to_pickle(f'{dataset}/ARI_df.pickle')
    NMI_df = pd.DataFrame(NMI_matrix, index=all_clusters.keys(), columns=all_clusters.keys())
    NMI_df.to_pickle(f'{dataset}/NMI_df.pickle')
    VI_df = pd.DataFrame(VI_matrix, index=all_clusters.keys(), columns=all_clusters.keys())
    VI_df.to_pickle(f'{dataset}/VI_df.pickle')

# get accuracy scores (with respect to annotation only)
if get_accuracy_scores:
    all_annotated,annotations = clean(gt_labels)
    gt_clusters = get_communities(annotations,all_annotated) 
    all_other_communities = {t:get_communities(l) for t,l in all_clusters.items() if t!='annotated'}
    conf_matrices = {gt_ids2labels[l]:{} for l in gt_clusters.keys()}
    for l,u in gt_clusters.items():
        h = gt_ids2labels[l]
        y_true = np.zeros(N)
        y_true[u] = 1
        y_true = y_true[all_annotated]
        for t,coms in all_other_communities.items():
            conf_matrices[h][t] = {}
            for k in sorted(coms.keys()):
                if k!=-1:
                    v = coms[k]
                    y_pred = np.zeros(N)
                    y_pred[v] = 1
                    y_pred = y_pred[all_annotated]
                    cm = confusion_matrix(y_true,y_pred)
                    conf_matrices[h][t][k] = {'TP':cm[1,1], 'TN':cm[0,0], 'FN':cm[1,0], 'FP':cm[0,1]}

    for t,coms in all_other_communities.items():
        keys = [k for k in sorted(coms.keys()) if k!=-1]
        prec = {}
        rec = {}
        spec = {}
        for l in gt_clusters.keys():
            h = gt_ids2labels[l]
            prec[h] = []
            rec[h] = []
            spec[h] = []
            for k in keys:
                prec[h].append(precision(conf_matrices[h][t][k]))
                rec[h].append(recall(conf_matrices[h][t][k]))
                spec[h].append(specificity(conf_matrices[h][t][k]))
        prec = pd.DataFrame(prec, index=keys)
        rec = pd.DataFrame(rec, index=keys)
        spec = pd.DataFrame(spec, index=keys)
        t = t.replace("$","").replace("\cap ","and").replace("\cup ","or").replace(' ','_')
        prec.to_pickle(f'{dataset}/precision_{t}.pickle')
        rec.to_pickle(f'{dataset}/recall_{t}.pickle')
        spec.to_pickle(f'{dataset}/specificity_{t}.pickle')
