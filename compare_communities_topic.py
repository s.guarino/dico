import numpy as np
import pickle
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.metrics import adjusted_mutual_info_score as AMI
from sklearn.metrics import adjusted_rand_score as ARI
from sklearn.metrics import normalized_mutual_info_score as NMI
from igraph import compare_communities
# from sklearn.metrics import normvariation_of_information as VI
from label_clusters import get_annotated_clusters
# from cdlib.classes import NodeClustering
from collections import Counter

####################### MACROS #########################
datasets = ['ita_elections','quirinale','crisi']
n = len(datasets)
########################################################

def precision(cm):
    return cm['TP']/max(1,(cm['TP']+cm['FP']))

def recall(cm):
    return cm['TP']/max(1,(cm['TP']+cm['FN']))

def specificity(cm):
    return cm['TN']/max(1,(cm['TN']+cm['FP']))

def VI(com1, com2):
    return compare_communities(com1, com2, method='vi')

def clean(com):
    ids = []
    labels = []
    for i,l in enumerate(com):
        if l!=-1:
            ids.append(i)
            labels.append(l)
    return ids,labels

def get_communities(labels, ids=None):
    if ids is None:
        ids = range(len(labels))
    coms = {}
    for i,l in zip(ids,labels):
        coms.setdefault(l,[]).append(i)
    return coms

def drop_small_clusters(labels, threshold=200):
    d = Counter(labels)
    to_drop = [h for h,u in d.items() if h!=-1 and u<200]
    v = np.array(labels)
    for h in to_drop:
        v[v==h] = -1
    return v

# read graph and clusters
clusters = {}
graphs = {}
name2id = {}
for dataset in datasets:
    with open(f'{dataset}/retweet_graph_with_clusters.pickle','rb') as f:
        G = pickle.load(f)
    graphs[dataset] = G
    name2id[dataset] = {name:i for i,name in enumerate(G.vs['name'])}
        # N = G.vcount()
    with open(f'{dataset}/projection_clusters.pickle','rb') as f:
        proj_clusters = pickle.load(f)
    with open(f'{dataset}/other_clusters.pickle','rb') as f:
        alg_clusters = pickle.load(f)
    gt_labels = get_annotated_clusters(G, 'annotated_communities/')
    gt_unique_labels = sorted(set(gt_labels))
    gt_labels2ids = {l:i for i,l in enumerate([h for h in gt_unique_labels if h!='na'])}
    gt_labels2ids['na'] = -1
    gt_ids2labels = {i:l for l,i in gt_labels2ids.items()}
    gt_labels = [gt_labels2ids[l] for l in gt_labels]
    for k,v in alg_clusters.items():
        alg_clusters[k] = drop_small_clusters(v)    
    all_clusters = {'annotated':gt_labels}
    all_clusters.update(proj_clusters)
    all_clusters.update(alg_clusters)
    # num_cluster_types = len(all_clusters)
    for k,v in all_clusters.items():
        if k not in clusters:
            clusters[k] = {}
        clusters[k][dataset] = v
in_common = {}
for i,d1 in enumerate(datasets):
    g1 = graphs[d1] 
    for j,d2 in enumerate(datasets[i+1:]):
        g2 = graphs[d2]
        common_names = list(set(g1.vs['name']).intersection(set(g2.vs['name'])))
        print(f'number of common users for {d1} and {d2}: {len(common_names)}')
        indices_1 = [name2id[d1][name] for name in common_names]
        indices_2 = [name2id[d2][name] for name in common_names]
        in_common[(d1,d2)] = (indices_1,indices_2)
        in_common[(d2,d1)] = (indices_2,indices_1)

# get mutual info scores
for k,v in clusters.items():
    print(f'creating matrices for {k}')
    n = len(v)
    AMI_matrix = np.zeros((n,n))
    ARI_matrix = np.zeros((n,n))
    NMI_matrix = np.zeros((n,n))
    VI_matrix = np.zeros((n,n))
    for i,(d1,m1) in enumerate(v.items()):
        m1 = np.asarray(m1)
        for j,(d2,m2) in enumerate(v.items()):
            if i == j:
                AMI_matrix[i,j] = 1
                ARI_matrix[i,j] = 1
                NMI_matrix[i,j] = 1
                VI_matrix[i,j] = 0
            else:
                m2 = np.asarray(m2)
                labels = m1[in_common[(d1,d2)][0]]
                l = m2[in_common[(d1,d2)][1]]
                # remove -1, i.e., unlabeled ids, for m1
                ids,labels = clean(labels)
                labels = np.asarray(labels)
                l = l[ids]
                AMI_matrix[i,j] = AMI(labels,l)
                ARI_matrix[i,j] = ARI(labels,l)
                NMI_matrix[i,j] = NMI(labels,l)
                for a,b in np.ndenumerate(np.unique(labels)):
                    labels[labels==b] = a
                for a,b in np.ndenumerate(np.unique(l)):
                    l[l==b] = a
                VI_matrix[i,j] = compare_communities(labels,l,method='vi')
    t = k
    t = t.replace("$","").replace("\cap ","and").replace("\cup ","or").replace(' ','_')
    AMI_df = pd.DataFrame(AMI_matrix, index=v.keys(), columns=v.keys())
    AMI_df.to_pickle(f'AMI_df_topic_{t}.pickle')
    ARI_df = pd.DataFrame(ARI_matrix, index=v.keys(), columns=v.keys())
    ARI_df.to_pickle(f'ARI_df_topic_{t}.pickle')
    NMI_df = pd.DataFrame(NMI_matrix, index=v.keys(), columns=v.keys())
    NMI_df.to_pickle(f'NMI_df_topic_{t}.pickle')
    VI_df = pd.DataFrame(VI_matrix, index=v.keys(), columns=v.keys())
    VI_df.to_pickle(f'VI_df_topic_{t}.pickle')

