import os
import igraph
import pickle
from argparse import ArgumentParser


def read_graph(fname):
    uid2vid = {}
    vid2uid = []
    n = 0
    edges = []
    ws = []
    with open(fname, 'r') as f:
        for line in f:
            s,t,attrs = line.split(' ',2)
            attrs = eval(attrs)
            sid = uid2vid.setdefault(s,n)
            if sid == n:
                vid2uid.append(s)
                n = n+1
            tid = uid2vid.setdefault(t,n)
            if tid == n:
                vid2uid.append(t)
                n = n+1
            edges.append((sid,tid))
            ws.append(attrs['weight'])
    G = igraph.Graph(edges=edges, directed=True, edge_attrs={'weight':ws},
                     vertex_attrs={'name':vid2uid})
    return G


def parse_args(): # -> tuple[str, str, int]:
    parser = ArgumentParser()
    parser.add_argument("dataset", type=str)
    args = parser.parse_args()
    return args.dataset


def main() -> None:
    dataset = parse_args()
    if os.path.exists(f'{dataset}/retweet_graph_with_clusters.pickle'):
        print(f'igraph file found at {dataset}/retweet_graph_with_clusters.pickle')
        return
    if os.path.exists(f'{dataset}/retweet_graph.pickle'):
        print(f'igraph file found at {dataset}/retweet_graph.pickle')
        return
    print(f'building igraph object for {dataset}')
    G = read_graph(f'{dataset}/global_RT_graph')
    print(f'dumping graph at {dataset}/retweet_graph.pickle')
    with open(f'{dataset}/retweet_graph.pickle', 'wb') as f:
        pickle.dump(G, f)   


if __name__ == "__main__":
    main()

