import json
import igraph
from pathlib import Path
from tqdm import tqdm
import numpy as np
from cdlib import algorithms, evaluation, viz, NodeClustering, ensemble
import pickle


ITA_DATASETS = ['ita_elections', 'crisi', 'quirinale']
WHICH_OTHERS = ['label propagation', 'infomap']


def read_graph(fname):
    uid2vid = {}
    vid2uid = []
    n = 0
    edges = []
    ws = []
    with open(fname, 'r') as f:
        for line in f:
            s,t,attrs = line.split(' ',2)
            attrs = eval(attrs)
            sid = uid2vid.setdefault(s,n)
            if sid == n:
                vid2uid.append(s)
                n = n+1
            tid = uid2vid.setdefault(t,n)
            if tid == n:
                vid2uid.append(t)
                n = n+1
            edges.append((sid,tid))
            ws.append(attrs['weight'])
    G = igraph.Graph(edges=edges, directed=True, edge_attrs={'weight':ws},
                     vertex_attrs={'name':vid2uid})
    return G


def get_membership(path, uid2vid):
    membership = -np.ones(len(uid2vid),dtype=int)
    for fname in path.iterdir():
        m = int(fname.stem[-1])
        if fname.as_posix().endswith('json'):
            with open(fname, 'r') as f:
                c = json.load(f)
            for k,v in c.items():
                s = v['author_id']
                membership[uid2vid[s]] = m
                ts = v['retweetter_id']
                for t in ts:
                    membership[uid2vid[t]] = m
        else:
            with open(fname, 'r') as f:
                for line in f:
                    s,t = line.split(' ', 2)[:2]
                    membership[uid2vid[s]] = m
                    membership[uid2vid[t]] = m
    return membership


def dump_projection_clusters():
    for d in ITA_DATASETS:
        print(f'loading retweet graph for {d}')
        try:
            G = igraph.read(f'{d}/retweet_graph_with_clusters.pickle')
        except:
            print(f'igraph graph not found for {d}, reloading it from scratch')
            root = Path(f"{d}/") 
            G = read_graph(root.joinpath('global_RT_graph'))
        uid2vid = {v['name']:v.index for v in G.vs}
        print(f'loading projection clusters for {d}')
        clusters = {}
        for path in tqdm(list(root.iterdir())):
            fname = path.stem
            if fname[0].isupper():
                proj = '$'+''.join([c for c in fname if c.isupper()])+'$'
                if len(proj)==5:
                    proj = proj.replace('O','\cup ')
                    proj = proj.replace('A','\cap ')
                clusters[proj] = get_membership(path, uid2vid)
                G.vs[proj] = clusters[proj]
        # print(f'finding and dumping common set of users for {d}')
        # users = np.where(np.min(list(clusters.values()),axis=0)>-1)[0].tolist()   
        # with open(f'{d}/projection_users.pickle', 'wb') as f:
        #     pickle.dump(users, f)
        # print(f'adapting clusters to the common set for {d}')
        # for k,m in clusters.items():
        #     m = np.asarray(m)[users].tolist()
        #     clusters[k] = m
        print(f'dumping graph (with clusters) and projection clusters for {d}')
        with open(f'{d}/retweet_graph_with_clusters.pickle', 'wb') as f:
            pickle.dump(G, f)   
        with open(f'{d}/projection_clusters.pickle', 'wb') as f:
            pickle.dump(clusters, f)

dump_projection_clusters()

