import os
from pathlib import Path
from tqdm import tqdm
import pickle
import pandas as pd
import numpy as np
##
import igraph
from cdlib import algorithms, evaluation, viz, NodeClustering, ensemble
from sklearn.metrics import adjusted_mutual_info_score as AMI
from sklearn.metrics import adjusted_rand_score as ARI
##
import matplotlib.pyplot as plt
import seaborn as sns
#
from graphmanager import communities_variation
#

ITA_DATASETS = ['ita_elections', 'crisi', 'quirinale']


# def AMI(c1, c2):
#     return (ami(c1,c2)+ami(c2,c1))/2


def read_graph(fname):
    uid2vid = {}
    vid2uid = []
    n = 0
    edges = []
    ws = []
    with open(fname, 'r') as f:
        for line in f:
            s,t,attrs = line.split(' ',2)
            attrs = eval(attrs)
            sid = uid2vid.setdefault(s,n)
            if sid == n:
                vid2uid.append(s)
                n = n+1
            tid = uid2vid.setdefault(t,n)
            if tid == n:
                vid2uid.append(t)
                n = n+1
            edges.append((sid,tid))
            ws.append(attrs['weight'])
    G = igraph.Graph(edges=edges, directed=True, edge_attrs={'weight':ws}, vertex_attrs={'name':vid2uid})
    return G


def get_annotated_clusters(G):
    uid2vid = {v['name']:v.index for v in G.vs}
    m = -np.ones(G.vcount())
    i = 0
    root = 'annotated_communities'
    for f in os.listdir(root):
        with open(os.path.join(root,f), 'r') as fin:
            d = eval(fin.read())
        users = []
        for u in d.keys():
            try:
                users.append(uid2vid[u])
            except:
                pass
        m[users] = i
        i += 1
    G.vs['annotated'] = m
    return m


def plot_scores_annotated():
    data = {'type':[], 'proj/alg':[], 'dataset':[], 'AMI':[], 'ARI':[]}
    for d in ITA_DATASETS:
        print(f'loading clusters for {d}')
        with open(f'{d}/projection_clusters.pickle', 'rb') as f:
            projection_clusters = pickle.load(f)
        with open(f'{d}/other_clusters.pickle', 'rb') as f:
            algorithmic_clusters = pickle.load(f)
        print(f'clusters found for {d}:')
        for k in projection_clusters.keys():
            print(f'\t{k}')
        for k in algorithmic_clusters.keys():
            print(f'\t{k}')
        try:
            with open(f'{d}/annotated_clusters.pickle', 'rb') as f:
                annotated = pickle.load(f)
        except:
            print(f'manually annotated clusters not found for {d}, reloading them')
            with open(f'{d}/retweet_graph_with_clusters.pickle', 'rb') as f:
                G = pickle.load(f)
            if 'annotated' in G.vertex_attributes():
                annotated = G.vs['annotated']
            else:
                annotated = get_annotated_clusters(G)
            with open(f'{d}/annotated_clusters.pickle', 'wb') as f:
                pickle.dump(annotated, f)
        annotated = np.asarray(annotated)
        print(f'creating AMI dataframe for {d}')
        # keys = list(clusters.keys())
        for k,m in projection_clusters.items():
            m = np.asarray(m)
            users = (annotated>=0)*(m>=0)
            m1 = annotated[users]
            m2 = m[users]
            this_ami = AMI(m1,m2)
            this_ari = ARI(m1,m2)
            data['type'].append('proj')
            data['proj/alg'].append(k)
            data['dataset'].append(d)
            data['AMI'].append(this_ami)
            data['ARI'].append(this_ari)
        for k,m in algorithmic_clusters.items():
            m = np.asarray(m)
            users = (annotated>=0)*(m>=0)
            m1 = annotated[users]
            m2 = m[users]
            this_ami = AMI(m1,m2)
            this_ari = ARI(m1,m2)
            data['type'].append('alg')
            data['proj/alg'].append(k)
            data['dataset'].append(d)
            data['AMI'].append(this_ami)
            data['ARI'].append(this_ari)
    data = pd.DataFrame(data)
    print(f'plotting AMI comparison between datasets to ami_comparison_catplot.png, ami_comparison_lineplot.png, ami_comparison_barplot.png and ami_comparison_heatmap.png')
    cp = sns.lineplot(data=data, x="dataset", y="AMI", markers=True, dashes=False, hue='type', style='proj/alg') #, kind="swarm", dodge=True) #, style='proj/alg')
    sns.move_legend(cp, "upper left", bbox_to_anchor=(1, 1))
    # cp = sns.scatterplot(data=data, x="dataset", y="AMI", markers=True, hue='type', style='proj/alg') #, kind="swarm", dodge=True) #, style='proj/alg')
    cp.figure.savefig('ami_comparison_catplot.png', bbox_inches='tight')
    # with open('ami_data.pickle', 'wb') as f:
    #     pickle.dump(data,f)
    plt.clf()
    lp = sns.lineplot(data=data, x='proj/alg', y='AMI', hue='dataset', sort=False)
    plt.xticks(rotation=45) 
    lp.figure.savefig('ami_comparison_lineplot.png', bbox_inches='tight')
    plt.clf()
    bp = sns.barplot(data=data, x='proj/alg', y='AMI', hue='dataset')
    bp.figure.savefig('ami_comparison_barplot.png', bbox_inches='tight')
    plt.clf()
    matrix = data.pivot_table(index='dataset', columns='proj/alg', values='AMI')
    hm = sns.heatmap(matrix, annot=True, fmt=".2f", square=True)
    hm.figure.savefig('ami_comparison_heatmap.png', bbox_inches='tight')
    print(f'plotting ARI comparison between datasets to ami_comparison_catplot.png, ami_comparison_lineplot.png, ami_comparison_barplot.png and ami_comparison_heatmap.png')
    cp = sns.lineplot(data=data, x="dataset", y="ARI", markers=True, dashes=False, hue='type', style='proj/alg') #, kind="swarm", dodge=True) #, style='proj/alg')
    sns.move_legend(cp, "upper left", bbox_to_anchor=(1, 1))
    # cp = sns.scatterplot(data=data, x="dataset", y="AMI", markers=True, hue='type', style='proj/alg') #, kind="swarm", dodge=True) #, style='proj/alg')
    cp.figure.savefig('ari_comparison_catplot.png', bbox_inches='tight')
    # with open('ami_data.pickle', 'wb') as f:
    #     pickle.dump(data,f)
    plt.clf()
    lp = sns.lineplot(data=data, x='proj/alg', y='ARI', hue='dataset', sort=False)
    plt.xticks(rotation=45) 
    lp.figure.savefig('ari_comparison_lineplot.png', bbox_inches='tight')
    plt.clf()
    bp = sns.barplot(data=data, x='proj/alg', y='ARI', hue='dataset')
    bp.figure.savefig('ari_comparison_barplot.png', bbox_inches='tight')
    plt.clf()
    matrix = data.pivot_table(index='dataset', columns='proj/alg', values='ARI')
    hm = sns.heatmap(matrix, annot=True, fmt=".2f", square=True)
    hm.figure.savefig('ari_comparison_heatmap.png', bbox_inches='tight')


def plot_ami_heatmap_per_dataset():
    for d in ITA_DATASETS:
        print(f'loading clusters for {d}')
        with open(f'{d}/projection_clusters.pickle', 'rb') as f:
            projection_clusters = pickle.load(f)
        with open(f'{d}/other_clusters.pickle', 'rb') as f:
            algorithmic_clusters = pickle.load(f)
        print(f'clusters found for {d}:')
        for k in projection_clusters.keys():
            print(f'\t{k}')
        for k in algorithmic_clusters.keys():
            print(f'\t{k}')
        try:
            with open(f'{d}/annotated_clusters.pickle', 'rb') as f:
                annotated = pickle.load(f)
        except:
            print(f'manually annotated clusters not found for {d}, reloading them')
            with open(f'{d}/retweet_graph_with_clusters.pickle', 'rb') as f:
                G = pickle.load(f)
            if 'annotated' in G.vertex_attributes():
                annotated = G.vs['annotated']
            else:
                annotated = get_annotated_clusters(G)
            with open(f'{d}/annotated_clusters.pickle', 'wb') as f:
                pickle.dump(annotated, f)
        annotated = np.asarray(annotated)
        print(f'creating AMI matrix for {d}')
        n = len(projection_clusters)+len(algorithmic_clusters)+1
        M = np.eye(n)
        keys = ['manual']+list(projection_clusters.keys())+list(algorithmic_clusters.keys())
        clusters = [annotated]+list(projection_clusters.values())+list(algorithmic_clusters.values())
        for i in range(n):
            m1 = np.asarray(clusters[i])
            for j in range(i+1,n):
                m2 = np.asarray(clusters[j])
                users = (m1>=0)*(m2>=0)
                _m1 = m1[users]
                _m2 = m2[users]
                M[i,j] = M[j,i] = AMI(_m1,_m2)
        print(f'plotting AMI heatmap for {d} at ami_heatmap.png')
        plt.clf()
        hm = sns.heatmap(M, annot=True, fmt=".2f", square=True, xticklabels=keys, yticklabels=keys, annot_kws={"size": 8})
        hm.figure.savefig(f'ami_heatmap_{d}.png', bbox_inches='tight')


def plot_ami_heatmap_per_algorithm():
    clusters = {}
    graphs = {}
    name2id = {}
    for d in ITA_DATASETS:
        graphs[d] = igraph.read(f'{d}/retweet_graph_with_clusters.pickle')
        name2id[d] = {name:i for i,name in enumerate(graphs[d].vs['name'])}
        print(f'loading clusters for {d}')
        with open(f'{d}/projection_clusters.pickle', 'rb') as f:
            projection_clusters = pickle.load(f)
        for k,m in projection_clusters.items():
            _d = clusters.setdefault(k,{})
            _d[d] = m
        with open(f'{d}/other_clusters.pickle', 'rb') as f:
            algorithmic_clusters = pickle.load(f)
        for k,m in algorithmic_clusters.items():
            _d = clusters.setdefault(k,{})
            _d[d] = m
        print(f'clusters found for {d}:')
        for k in projection_clusters.keys():
            print(f'\t{k}')
        for k in algorithmic_clusters.keys():
            print(f'\t{k}')
        try:
            with open(f'{d}/annotated_clusters.pickle', 'rb') as f:
                annotated = pickle.load(f)
        except:
            print(f'manually annotated clusters not found for {d}, reloading them')
            with open(f'{d}/retweet_graph_with_clusters.pickle', 'rb') as f:
                G = pickle.load(f)
            if 'annotated' in G.vertex_attributes():
                annotated = G.vs['annotated']
            else:
                annotated = get_annotated_clusters(G)
            with open(f'{d}/annotated_clusters.pickle', 'wb') as f:
                pickle.dump(annotated, f)
        annotated = np.asarray(annotated)
        _d = clusters.setdefault('manual',{})
        _d[d] = annotated
    
    in_common = {}
    for i,d1 in enumerate(ITA_DATASETS):
        g1 = graphs[d1] 
        for j,d2 in enumerate(ITA_DATASETS[i+1:]):
            g2 = graphs[d2]
            common_names = list(set(g1.vs['name']).intersection(set(g2.vs['name'])))
            print(f'number of common users for {d1} and {d2}: {len(common_names)}')
            indices_1 = [name2id[d1][name] for name in common_names]
            indices_2 = [name2id[d2][name] for name in common_names]
            in_common[(d1,d2)] = (indices_1,indices_2)

    for k,cls in clusters.items():
        print(f'creating AMI matrix for {k}')
        n = len(ITA_DATASETS)
        M = np.eye(n)
        for i,d1 in enumerate(ITA_DATASETS):
            m1 = np.asarray(cls[d1])
            for j,d2 in enumerate(ITA_DATASETS[i+1:]):
                m2 = np.asarray(cls[d2])
                _m1 = m1[in_common[(d1,d2)][0]]
                _m2 = m2[in_common[(d1,d2)][1]]
                users = (_m1>=0)*(_m2>=0)
                _m1 = _m1[users]
                _m2 = _m2[users]
                print('normalized:')
                print(communities_variation([_m1,_m2],normalize=True))
                print('non normalized:')
                print(communities_variation([_m1,_m2],normalize=False))
                print('AMI with arithmetic:')
                print(AMI(_m1,_m2))
                print('AMI with max:')
                print(AMI(_m1,_m2,average_method='max'))
                return

                M[i,j] = M[j,i] = AMI(_m1,_m2)
        print(f'plotting AMI heatmap for {k} at ami_heatmap_{k}.png')
        plt.clf()
        hm = sns.heatmap(M, annot=True, fmt=".2f", square=True, xticklabels=ITA_DATASETS, yticklabels=ITA_DATASETS, annot_kws={"size": 8})
        hm.figure.savefig(f'ami_heatmap_{k}.png', bbox_inches='tight')

# plot_ami_annotated()
# plot_ami_heatmap_per_dataset()
plot_ami_heatmap_per_algorithm()
