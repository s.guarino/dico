import json
import igraph
from pathlib import Path
from tqdm import tqdm
import numpy as np
from cdlib.utils import convert_graph_formats
from cdlib import algorithms #, evaluation, viz, NodeClustering, ensemble
import networkx as nx
import pickle
from time import time
from argparse import ArgumentParser


ITA_DATASETS = ['ita_elections', 'crisi', 'quirinale']
WHICH_OTHERS = ['louvain', 'label propagation', 'infomap'] #, 'walktrap', 'infomap']


def read_graph(fname):
    uid2vid = {}
    vid2uid = []
    n = 0
    edges = []
    ws = []
    with open(fname, 'r') as f:
        for line in f:
            s,t,attrs = line.split(' ',2)
            attrs = eval(attrs)
            sid = uid2vid.setdefault(s,n)
            if sid == n:
                vid2uid.append(s)
                n = n+1
            tid = uid2vid.setdefault(t,n)
            if tid == n:
                vid2uid.append(t)
                n = n+1
            edges.append((sid,tid))
            ws.append(attrs['weight'])
    G = igraph.Graph(edges=edges, directed=True, edge_attrs={'weight':ws},
                     vertex_attrs={'name':vid2uid})
    return G


def randomized_louvain(G, n=10):
    i = 0
    best_mod = 0
    inv_p = np.empty(G.vcount())
    while True:
        p = np.random.permutation(range(G.vcount()))
        H = G.permute_vertices(p.tolist())
        cl = H.community_multilevel(weights='weight')
        mod = cl.modularity
        if mod>best_mod:
            best_mod = mod
            m = np.asarray(cl.membership)[p]
        i += 1
        if i == n:
            break
    return m


def get_clusters(G, users=None, which='all'):
    clusters = {}
    if which == 'all':
        which = WHICH_OTHERS
    elif isinstance(which, str):
        which = [which]
    for alg in which:
        try:
            m = G.vs[alg]
            print(f'communities found for algorithm {alg}: skipping it!') 
        except:
            if alg == 'walktrap':
                ### walktrap ###
                print('computing walktrap communities')
                ts = time()
                m = G.community_walktrap(weights='weight').as_clustering().membership
                print(f'communities computed in time {time()-ts}')
                ################
            elif alg == 'infomap':
                ### infomap ###
                print('computing infomap communities')
                ts = time()
                G_nx = convert_graph_formats(G, nx.Graph, directed=G.is_directed())
                m = algorithms.infomap(G_nx).to_node_community_map()
                m = [m[n][0] for n in G.vs['name']]
                # m = G.community_infomap(edge_weights='weight').membership
                print(f'communities computed in time {time()-ts}')
                ###############
            elif alg == 'louvain':
                ### louvain ###
                print('computing louvain communities (best out of 10 randomized runs)')
                ts = time()
                H = G.as_undirected(combine_edges='sum')
                m = randomized_louvain(H)
                print(f'communities computed in time {time()-ts}')
                ###############
            elif alg == 'label propagation':
                ### label propagation ###
                print('computing label propagation communities')
                ts = time()
                H = G.as_undirected(combine_edges='sum')
                m = H.community_label_propagation(weights='weight').membership
                print(f'communities computed in time {time()-ts}')
            else:
                print(f'error: algorithm {alg} is not supported and will be ignored!') 
                continue
            G.vs[alg] = m 
        if users is not None:
            m = np.asarray(m)[users].tolist()
        clusters[alg] = m
    #########################
    # membership = -np.ones(G.vcount())
    # for i,c in enumerate(coms.communities):
    #     membership[com] = i
    return G,clusters 

    
def dump_other_clusters(d, algorithm):
    print(f'loading retweet graph for {d}')
    try:
        G = igraph.read(f'{d}/retweet_graph_with_clusters.pickle')
    except:
        try:
            G = igraph.read(f'{d}/retweet_graph.pickle')
        except:
            print(f'igraph graph not found for {d}, reloading it from scratch')
            root = Path(f"{d}/") 
            G = read_graph(root.joinpath('global_RT_graph'))
    # print(f'loading common set of users for {d}')
    # with open(f'{d}/projection_users.pickle', 'rb') as f:
    #     users = pickle.load(f)
    print(f'computing clusters for {d}')
    G,clusters = get_clusters(G, which=algorithm)
    print(f'dumping graph (with clusters) and clusters for {d}')
    with open(f'{d}/retweet_graph_with_clusters.pickle', 'wb') as f:
        pickle.dump(G, f)   
    with open(f'{d}/other_clusters.pickle', 'wb') as f:
        pickle.dump(clusters, f)   
            

def parse_args(): # -> tuple[str, str, int]:
    parser = ArgumentParser()
    parser.add_argument("dataset", type=str)
    parser.add_argument("--algorithm", type=str, required=False, default=WHICH_OTHERS)
    args = parser.parse_args()
    return args.dataset, args.algorithm


def main() -> None:
    dataset, algorithm = parse_args()
    # print(dataset, algorithm)
    dump_other_clusters(dataset, algorithm)


if __name__ == "__main__":
    main()

